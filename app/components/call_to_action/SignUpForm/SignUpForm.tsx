'use client'

import { FormEvent, useState } from "react";
import styles from './SignUpForm.module.css';
import action_styles from '../Button.module.css'

export default function SignUpForm() {
    function signUp(event: FormEvent<HTMLFormElement>): void {
        event.preventDefault();
        setSignedUp(true);
    }
    const [signedUp, setSignedUp] = useState(false);
    if(!signedUp) {
        return (
            <form className={styles.form} onSubmit={signUp}>
                    <input name="email" placeholder="you@example.org" type="email" required/>
                    <input className={action_styles.action_button} type="submit" value="Sign Up" />
            </form>
        );
    } else {
        return (
        <div className="user_submitted_form">
            <h2>Thanks for signing up to our newsletter.</h2>
            <p>If this was a real website, you would now be recieving our weekly newsletter.</p>
            <p>But, unfortunately, this is just a hobby frontend project.</p>
        </div>);
    }
}
