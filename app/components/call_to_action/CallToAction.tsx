import { ReactNode } from 'react';
import styles from './CallToAction.module.css'
export default function CallToAction({children, heading, id = ""}: {children: ReactNode, heading: string, id?:string}) {

    return (
        <div id={id} className={styles.container}>
            <h1 className={styles.title}>{heading}</h1>
            {children}
        </div>
    );
}