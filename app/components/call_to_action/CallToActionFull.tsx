import CallToAction from "./CallToAction";
import FullForm from "./FullForm/FullForm";

export default function CallToActionFull() {
    return (
        <CallToAction id="get_involved"  heading="Get in touch">
            <FullForm/>
        </CallToAction>
    );
}