import CallToAction from "./CallToAction";
import SignUpForm from "./SignUpForm/SignUpForm";

export default function CallToActionForm() {
    return(
        <CallToAction heading="Join our furry family's newsletter today">
            <SignUpForm/>
        </CallToAction>
    );
}