'use client'
import { ChangeEvent, FormEvent, useState } from "react";
import styles from "./FullForm.module.css";


export default function FullForm() {

    const [isMessageSent, setMessageSent] = useState(false);
    const [userName, setUserName] = useState("");
    function sendMessage(event: FormEvent<HTMLFormElement>):void {
        event.preventDefault();
        setMessageSent(true);
    }

    function nameChanged(event: ChangeEvent<HTMLInputElement>): void {
        const name_lovercase:string = event.target.value;
        const name = name_lovercase.charAt(0).toUpperCase() + name_lovercase.slice(1);
        setUserName(name);
    }

    if(!isMessageSent) {
        return (
            <form className={styles.container} onSubmit={sendMessage}>
            <input className={styles.name} type="text" placeholder="Name" required onChange={nameChanged}/>
            <input className={styles.email} type="email" placeholder="Email" required/>
            <input type="textarea" placeholder="Message" required/>
            <input className={styles.submit} type="submit" value="Send"/>
        </form>
        );
    }else {
        return (
            <div className="user_submitted_form">
                <h2>Thanks for contacting us, {userName}.</h2>
                <p>If this was a real website</p>
                <p>You&#39;ll be getting a reply from one of our friendly volunteers soon</p>
                <p>But, unfortunately, this is just a hobby frontend project.</p>
            </div>
        );
    }
}
