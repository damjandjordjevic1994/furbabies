import Witness from "./witness/Witness";
import WitnessType from "./witness/WitnessType";
import styles from "./Testimonies.module.css";

const witnesses: WitnessType[] = [
    {
        testemonial: "FurEver is the best place to find your new furry friend. I couldn't be happier with my adoption experience!",
        name: "Samantha",
        image_url: "testimonies/samantha.jpg",
    },
    {
        testemonial: "The team at FurEver is incredibly dedicated and caring. I'm grateful for the love they've shown to all the animals.",
        name: "Ethan",
        image_url: "testimonies/ethan.jpg",
    },
    {
        testemonial: "I've volunteered at many shelters, but FurEver has a special place in my heart. Their commitment to animal welfare is unmatched.",
        name: "Olivia",
        image_url: "testimonies/olivia.jpg",
    },  
]
export default function Testimonies() {
    return (
        <div className="small_width_wrapper">
            <div className={styles.container}>
                <h1 className={styles.title}>Pawsome</h1>
                <div className={styles.witness_container}>
                    {
                        witnesses.map((witness) =>
                            <Witness key={witness.name} {...witness}/>
                        )
                    }
                </div>
            </div>
        </div>
    );
}