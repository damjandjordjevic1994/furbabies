import WitnessType from "./WitnessType";
import styles from "./Witness.module.css"

export default function Witness({testemonial, name, image_url}: WitnessType) {
    return (
        <div className={styles.container}>
            <p className={styles.testimonial}>{testemonial}</p>
            <img className={styles.image} src={image_url} alt={`${name}'s photo`} />
            <h3 className={styles.name}>{name}</h3>
        </div>
    );
}