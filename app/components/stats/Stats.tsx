import StatItem from "./stat_item/StatItem";
import StatItemType from "./stat_item/StatItemType";
import styles from './Stats.module.css';

const statistics: StatItemType[] = [
    {
        count: 1000,
        message: "Happy tails",
    },
    {
        count: 500,
        message: "Furry friends adopted",
    },
    {
        count: 200,
        message: "Dedicated volunteers",
    },
];

export default function Stats() {
    return (
        <div className={styles.container}>
            {statistics.map((stat) => <StatItem key={stat.message} {...stat}/>)}
        </div>
    );
}