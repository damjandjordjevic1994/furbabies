import StatItemType from "./StatItemType";
import styles from './StatItem.module.css'
export default function StatItem({count, message}: StatItemType) {
    return (
        <div className={styles.container}>
            <h1 className={styles.title}>{count}+</h1>
            <p className={styles.message}>{message}</p>
        </div>
    );
}