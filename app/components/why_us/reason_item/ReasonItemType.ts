export default interface ReasonItemType{
  title: string,
  content: string,
}
