import ReasonItemType from "./ReasonItemType";
import styles from './ReasonItem.module.css'
export default function ReasonItem({title, content}: ReasonItemType) {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>{title}</h1>
      <p className={styles.content}>{content}</p>
    </div>
  );
}
