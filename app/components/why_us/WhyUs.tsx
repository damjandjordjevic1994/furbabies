import ReasonItem from "./reason_item/ReasonItem";
import ReasonItemType from "./reason_item/ReasonItemType";
import styles from './WhyUs.module.css';
const reasons: ReasonItemType[] = [
  {
    title: "Community",
    content: "Join a vibrant community of animal lovers who are dedicated to making a difference in the lives of cats and dogs.",
  },
  {
    title: "Love & Care",
    content: "Experience the love, care, and compassion that defines the FurEver family. Every furry friend is treated with the utmost kindness.",
  },
  {
    title: "Adoption",
    content: "Find your perfect furry match and open your heart and home to a loving companion. Our adoption process is designed to make tails wag!",
  },
  {
    title: "Volunteer",
    content: "Become a part of our furry family by volunteering your time and skills to create a better world for animals in need.",
  },
];

export default function WhyUs() {
  return (
    <div id="adopt" className="small_width_wrapper">
      <div className={styles.container}>
        <div className={styles.heading}>
          <h1 className={styles.title}>Why FurEver?</h1>
          <p className={styles.content}>Discover the reasons why FurEver is the ultimate destination for animal lovers and furry friends.</p>
        </div>
        {reasons.map((reason) => <ReasonItem key={reason.title} {...reason} />)}
      </div>
    </div>
  );
}
