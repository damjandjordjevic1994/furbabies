export default interface Person {
    name: string;
    job_title: string;
    image_uri: string;
}