import PersonType from './PersonType'

import styles from './Person.module.css';

export default function Person({name, job_title, image_uri}: PersonType) {
    return (
        <div className={styles.container}>
            <img className={styles.image} src={image_uri} alt={`${name}'s photo`}/>
            <h3 className={styles.name}>{name}</h3>
            <p className={styles.job_title}>{job_title}</p>
        </div>
    );
}