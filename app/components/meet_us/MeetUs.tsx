import Person from './person/Person'
import PersonType from './person/PersonType'
import styles from './MeetUs.module.css'
const people: PersonType[] = [
    {
        name: "Luna",
        job_title: "Founder & animal whisperer",
        image_uri: "meet_us/luna.jpg"
    },
    {
        name: "Max",
        job_title: "Chief cuddle officer",
        image_uri: "meet_us/max.jpg"
    },
    {
        name: "Bella",
        job_title: "Purr-fect adoption specialist",
        image_uri: "meet_us/bella.jpg"
    },
    {
        name: "Leo",
        job_title: "Doggo caretaker",
        image_uri: "meet_us/leo.jpg"
    }
]
export default function MeetUs() {

    return (
        <div id="team">
            <h1 className={styles.title} >Meet Us</h1>
            <div className={styles.container}>
                {people.map((person) => 
                    <Person {...person} key={person.name}/>
                )}
            </div>
            
        </div>
    );
}
