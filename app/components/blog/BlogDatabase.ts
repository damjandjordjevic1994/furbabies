import BlogItemType from "./blog_item/BlogItemType";

export const blog_items: BlogItemType[] = [
    {
      name: "Finding your perfect match",
      category: "Adoption",
      image_url: "blog/adoption.jpg",
      content: [
        "Once upon a time, nestled within the dense foliage of a sprawling forest, there lived a tiny, trembling kitten named Onja. His fur was as black as the midnight sky, and his eyes, wide with fear, mirrored the uncertainty of his young heart. Onja had found himself abandoned, callously discarded in the heart of the woods by an unknown passerby.",
        "Alone and bewildered, Onja meandered through the undergrowth, his small paws padding softly against the forest floor. Hunger gnawed at his belly, and fear gripped his soul as the shadows of the towering trees loomed ominously overhead.",
        "Days turned into nights, and Onja' plight seemed dire until one fateful morning when he stumbled upon a clearing where a group of strangers had set up camp. With a glimmer of hope sparking within him, Onja approached cautiously, his heart pounding with trepidation.",
        "The strangers, a band of travelers weary from their journey, noticed the tiny kitten and took pity on him. They offered him food and water, their kindness warming Onja' soul like the rays of the morning sun. In return, Onja showered them with affection, his purrs a melody of gratitude echoing through the camp.",
        "As days turned into weeks, Onja endeared himself to the travelers, his playful antics bringing joy to their weary hearts. But beneath his innocent facade lay a fierce determination, for Onja had a mission – to repay the kindness shown to him by his newfound friends.",
        "With every passing day, Onja honed his hunting skills, prowling through the forest with stealth and cunning. His sharp claws and keen senses proved invaluable as he waged war against the local mouse population, his ferocity unmatched by any creature in the woods.",
        "Under the cover of night, Onja stalked his prey with silent precision, his eyes gleaming with determination as he hunted down the rodents that plagued the camp. With each mouse he caught, Onja ensured the safety of his adopted family, his loyalty unwavering in the face of adversity.",
        "And so, as the seasons changed and the forest flourished, Onja emerged as a hero among his companions, his bravery and loyalty earning him a place of honor in their hearts. No longer was he just a frightened kitten lost in the woods, but a valiant protector, a guardian angel watching over those who had shown him kindness in his darkest hour.",
        "As the years passed, Onja' legend grew, his tale whispered among the trees as a testament to the enduring power of compassion and the indomitable spirit of a tiny kitten who had conquered the wilderness with nothing but love in his heart. And though he may have been thrown into the woods by fate's cruel hand, Onja had found his true home among friends who had become family, his legacy forever etched into the fabric of the forest where he roamed free, a fearless hunter and a loyal companion till the end of his days.",
      ],
    },
    {
      name: "A tale of hope",
      category: "Rescue",
      image_url: "blog/rescue.jpg",
      content: [
        "In the heart of a winter storm, where the world seemed to be blanketed in an endless expanse of white, two small pups found themselves lost in the midst of the swirling snow. They were brothers, their fluffy fur barely visible against the backdrop of the storm. With each passing moment, the cold seemed to seep deeper into their bones, and despair threatened to overwhelm them.",
        "But in the nearby village, there lived a kind-hearted soul named Anna. Despite the howling winds and biting cold, Anna couldn't shake the feeling that someone out there needed her help. So, bundling up in her warmest coat and donning her trusty snow boots, she set out into the blizzard, determined to make a difference.",
        "As she trudged through the knee-deep snow, Anna followed the faint sound of whimpering until she stumbled upon the two pups huddled together beneath a snow-covered bush. Without a moment's hesitation, she scooped them up in her arms, feeling their shivering bodies against her own.",
        "Braving the elements, Anna made her way back to her cozy cottage, where a roaring fire crackled in the hearth. She wrapped the pups in thick blankets, gently warming them by the fire's comforting glow. Slowly but surely, their shivers began to subside, replaced by a sense of safety and security.",
        "As the storm raged on outside, Anna nursed the pups back to health with warm milk and tender care. With each passing day, they grew stronger and more energetic, their playful barks filling the cottage with joy.",
        "Eventually, the storm passed, leaving behind a pristine landscape glistening in the sunlight. It was a new beginning for the two pups, who had found not only a loving home but also a lifelong friend in Anna.",
        "From that day forward, the pups never strayed far from Anna's side, their tails wagging in gratitude for the second chance she had given them. And as they frolicked through the meadows, chasing after snowflakes and basking in the warmth of the sun, they knew that no matter what challenges lay ahead, they would always have each other—and the unwavering hope that had led them out of the cold and into the light.",
      ], 
    },
    {
      name: "Unconditional bonds",
      category: "Love",
      image_url: "blog/love.jpeg",
      content: [
        "In the rolling hills of the countryside, there lived a horse named Spirit and his devoted owner, Emma. They shared a bond forged through years of companionship and trust, navigating life's challenges together.",
        "One sunny afternoon, as Emma saddled up Spirit for a ride through the meadows, she felt a sudden pain shoot through her leg. She winced, struggling to mount her beloved horse. Concern flashed in Spirit's deep, soulful eyes as he sensed Emma's discomfort.",
        "Ignoring her own pain, Emma urged Spirit forward, determined to enjoy their ride. But as they trotted along the familiar paths, Emma's discomfort grew unbearable, her every movement a struggle.",
        "Sensing his owner's distress, Spirit slowed his pace, nuzzling Emma's shoulder gently as if to offer his support. Emma's heart swelled with gratitude for her faithful companion's empathy.",
        "Realizing that Emma needed help, Spirit gently nudged her towards a nearby tree, where she could lean against its sturdy trunk and rest. With a soft whinny, Spirit stood patiently by her side, his warm breath mingling with hers in the crisp winter air.",
        "As Emma took a moment to catch her breath, she felt a surge of emotion wash over her. Despite her pain, she couldn't help but marvel at the depth of Spirit's love and understanding.",
        "With Spirit's unwavering support, Emma found the strength to carry on, her heart filled with gratitude for her loyal friend. And as they made their way back home, Emma knew that no matter what challenges life threw their way, she could always count on Spirit's boundless love to see her through.",
      ],  
    },
    {
      name: "Tails of tail wags",
      category: "Joy",
      image_url: "blog/joy.jpg",
      content: [
        "In the heart of Rottnest Island, amidst the eucalyptus trees and coastal dunes, lived a quokka named Ruby. Ruby was a spirited creature, known for her playful antics and boundless energy. But one fateful day, while exploring the island's rugged terrain, Ruby found herself in a perilous situation.",
        "As she hopped along a narrow trail near the cliffs, Ruby misjudged her footing and tumbled down a steep embankment, landing with a thud at the bottom. Pain shot through her leg as she struggled to stand, her heart pounding with fear as she realized she was unable to move.",
        "Hours passed, and Ruby lay there, alone and afraid, her cries for help lost in the rustle of the wind and the crash of the waves below. But just when she thought all hope was lost, a pair of hikers stumbled upon her, their faces filled with concern at the sight of the injured quokka.",
        "\"We need to help her,\" one of the hikers said, kneeling down beside Ruby and gently stroking her fur. \"She looks hurt.\"",
        "With careful hands, the hikers fashioned a makeshift stretcher out of branches and leaves, carefully lifting Ruby onto it before beginning the arduous trek back to civilization. Despite the pain radiating through her body, Ruby felt a flicker of hope ignite within her as she realized she wasn't alone—that someone cared enough to come to her aid.",
        "Eventually, they reached the ranger station, where a team of wildlife experts and veterinarians were waiting to lend their assistance. With gentle hands and compassionate hearts, they examined Ruby's injuries, treating her with the care and attention she so desperately needed.",
        "Days turned into weeks as Ruby recuperated in the safety of the ranger station, surrounded by people who had become like family to her. With each passing day, her strength returned, and her spirits lifted as she realized just how fortunate she was to have been rescued.",
        "Finally, the day came when Ruby was deemed fit to return to the wild. With a mixture of excitement and trepidation, she bid farewell to her newfound friends, gratitude shining in her eyes as she hopped out into the sunlight once more.",
        "As Ruby bounded through the forest, her heart brimming with joy and gratitude, she knew that she would never forget the kindness that had been shown to her in her time of need. And as she disappeared into the dense foliage, her infectious smile lighting up the world around her, she carried with her a newfound appreciation for the power of compassion—and the unwavering hope that had guided her through the darkest of times.",
      ], 
    },  
  ];