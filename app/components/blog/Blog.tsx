import BlogItem from "./blog_item/BlogItem";
import { blog_items } from "./BlogDatabase";
import styles from "./Blog.module.css";
export default function Blog() {
  return (
    <div id="blog" className={styles.container}>
      <h2 className={styles.title}>FurEver blog</h2>
      <div className={styles.blog_item_container}>
        {blog_items.map((item, i) => {
          return <BlogItem key={item.category} {...item} index={i}/>;
        })}
      </div>
    </div>
  );
}
