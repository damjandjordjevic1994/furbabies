import BlogItemType from "./BlogItemType";
import styles from './BlogItem.module.css'
import JumpyLabel from "./JumpyLabel";
export default function BlogItem({name, category, image_url, content, index}: BlogItemType & {index: number}) {
  return (
    <div className={styles.container}>
      <label htmlFor={`checkbox-${index}`} className={[styles.button, styles.close].join(' ')}>X</label>
      <input id={`checkbox-${index}`} type="checkbox" className={styles.input}/>
      <img className={styles.image} src={image_url} alt={`${category} image`}/>
      <div className={styles.content}>
        {content.map((paragraph, i) => <p key={i}>{paragraph}</p>)}
      </div>
      <p className={styles.category} >{category}</p>
      <h2 className={styles.name}>{name}</h2>
      <JumpyLabel index={index} className={[styles.button, styles.open].join(' ')}/>
    </div>
  );
}
