'use client'
export default function JumpyLabel({index, className}: {index: number, className: string}) {
    return (
        <label className={className} htmlFor={`checkbox-${index}`}  onClick={() => window.location.href="#blog"}>Read</label>
    );
}