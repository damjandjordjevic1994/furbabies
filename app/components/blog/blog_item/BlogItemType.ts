export default interface BlogItemType {
	name: string,
	category: string,
	image_url: string,
	content: string[],
};
