import styles from './Faq.module.css';
import FaqItem from './faq_item/FaqItem';
import FaqItemType from './faq_item/FaqItemType';
const faq_items: FaqItemType[] = [
    {
        question: "How can I adopt a pet?",
        answer: "To adopt a pet from FurEver, simply visit our shelter and meet our adorable furry friends. Our adoption specialists will guide you through the process and help you find the perfect match for your family.",
    },
    {
        question: "Can I volunteer at FurEver?",
        answer: "Absolutely! We welcome volunteers with open arms. Whether you want to help with daily care, socialization, or events, there's a place for you in our furry family.",
    },
    {
        question: "Do you offer fostering programs?",
        answer: "Yes, we have a dedicated fostering program that allows individuals to provide temporary care for animals in need. It's a rewarding experience that makes a huge difference in the lives of our furry friends.",
    },
    {
        question: "What are your adoption fees?", 
        answer: "Our adoption fees vary based on the age and type of animal. Rest assured, every fee goes towards the well-being and care of our animals, ensuring they receive the love and attention they deserve.",
    },
    {
        question: "How can I support FurEver?",
        answer: "You can support FurEver by donating, volunteering, fostering, or simply spreading the word about our mission. Every little bit helps in creating a better world for our furry companions.",
    },
]

export default function Faq() {
    return (
        <div className="small_width_wrapper">
            <div id="faq" className={styles.container}>
                <h1 className={styles.title}>FurEver FAQs</h1>
                {
                    faq_items.map((item) => <FaqItem key={item.question} {...item} />)
                }
            </div>
        </div>
    );
}