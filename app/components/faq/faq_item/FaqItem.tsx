'use client'
import FaqItemType from "./FaqItemType";
import styles from "./FaqItem.module.css";
import { useState } from "react";

export default function FaqItem({question, answer}: FaqItemType) {
    const [isExpanded, setExpanded] = useState(false)
    return (
        <div className={styles.container} onClick={() => setExpanded((prev) => !prev)}>
            <div className={styles.question}>
                <p>{question}</p>
                <p className={[styles.arrow , isExpanded && styles.up_side_down].join(' ')}>↓</p>
            </div>
            <p className={[styles.answer, isExpanded? styles.expanded:''].join(' ')}>{answer}</p>
        </div>
    );
}