import SocialLink from "./social_link/SocialLink";
import SocialLinkType from "./social_link/SocialLinkType";
import styles from './Footer.module.css';
import dotenv from 'dotenv'
dotenv.config();

const socialMedia: SocialLinkType[] = [
    {
        title: "Github",
        url: process.env.GITHUB || "NO LINK FOUND",
        image_url: "/footer/github-mark.svg",
        image_alt:"github",
    },
    {
        title: "GitLab",
        url: process.env.GITLAB || "NO LINK FOUND",
        image_url: "/footer/gitlab.svg",
        image_alt:"gitlab",
    }
];
export default function Footer() {
    return (
        <footer className={styles.container}>
            <h1 className={styles.title} >Take a look at my other work</h1>
            <div className={styles.link_container}>
                {socialMedia.map((item) => <SocialLink key={item.title} {...item}/>)}
            </div>
        </footer>
    );
}
