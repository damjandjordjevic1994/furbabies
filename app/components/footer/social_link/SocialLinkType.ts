export default interface SocialLinkType {
    title: string,
    url: string,
    image_url: string,
    image_alt: string
}