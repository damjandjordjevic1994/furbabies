import SocialLinkType from "./SocialLinkType";
import styles from "./SocialLink.module.css"
export default function SocialLink({title, url, image_url, image_alt}: SocialLinkType) {
    return (
        <a className={styles.container} href={url} title={title}>
            <img className={styles.image} src={image_url} alt={image_alt} />
        </a>
    );
}