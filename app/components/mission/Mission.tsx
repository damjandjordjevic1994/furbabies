import styles from './Mission.module.css';

export default function Mission() {
    return (
    <div className='small_width_wrapper'>
        <div className={styles.container}>
            <div className={styles.image_container}>
                <img className={styles.image} src="/mission/cat.jpg" alt="cat"/>
                <img className={styles.image} src="/mission/parrot.jpg" alt="parrot" />
            </div>
            <div className={styles.text_container}>
                <h2 className={styles.title}>Our Mission</h2>
                <p>At FurEver, we&#39re on a mission to provide a loving and caring environment for cats and dogs in need. We believe that every animal deserves a chance to find a forever home, and we&#39re dedicated to making that happen.</p>
                <p>We&#39re not just an animal shelter; we&#39re a community of animal lovers who are passionate about making a difference. Whether it&#39s through fostering, volunteering, or adopting, there are countless ways to get involved and be a part of our furry family.</p>
                <p>Join us in creating a world where every wagging tail and purring friend finds the love and care they deserve. Together, we can make a paw-sitive impact on the lives of these adorable fur babies. </p>
            </div>
        </div>
    </div>
    );
}