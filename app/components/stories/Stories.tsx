import styles from './Stories.module.css';

export default function Stories() {
    return (
        <div className='small_width_wrapper'>
            <div  id="about" className={styles.container}>
                <img className={styles.image} src="stories/cute_animal.jpg" alt="cute animal" />
                <div className={styles.text_container}>
                    <h1 className={styles.title} >FurEver stories</h1>
                    <p>Every animal has a unique story, and at FurEver, we&#39re dedicated to sharing those stories with the world. From heartwarming adoption tales to inspiring rescue missions, our blog is filled with the magic of furry friendships.</p>
                    <p>Join us in celebrating the joy of pet companionship and the incredible bond between humans and animals. Get ready to embark on a journey of love, laughter, and endless wagging tails!</p>
                    <p>At FurEver, we believe that every animal deserves to be cherished, and our stories reflect the beauty of that belief. Get ready to be moved, inspired, and filled with the warmth of unconditional love.</p>
                </div>
            </div>
        </div>
    );
}