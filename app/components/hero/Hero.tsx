'use client';

import { useState } from 'react';
import styles from './Hero.module.css'


export default function Hero() {
  interface Link {
    name: string;
    url: string;
  }
  const links: Link[] = [
    {
      name: "About",
      url: "#about"
    },
    {
      name: "Team",
      url: "#team"
    },
    {
      name: "Adopt",
      url: "#adopt"
    },
    {
      name: "Get Involved",
      url: "#get_involved"
    },
  ]

  const [isExpanded, setExpanded] = useState(false);
  
  return (
    <div className={styles.container} >
      <img className={styles.background} src='/hero/puppies.jpg' alt=""/>
      <div className={styles.hero}>
        <div className={styles.title_bar_container}>
          <div className={styles.title_bar}>
            <div className={styles.logo}>
              <img className={styles.title_bar_image} src='hero/mouse_eating.jpg' alt=""/>
              <h1>FurEver</h1>
            </div>
            <label className={[styles.transitions, styles.hamburger_menu, isExpanded && styles.hamburger_menu_expanded].join(' ')}>
              <input onClick={() => {setExpanded((prev) => !prev)}} type="button" />
            </label>
            <ul className={[styles.transitions, styles.aside, isExpanded && styles.aside_shown].join(' ')}>
                
                {links.map((link) => 
                    <li key={link.name}  >
                      <a onClick={() => setExpanded(false)} className={styles.link} href={link.url}>{link.name}</a>
                    </li>
                )}
                
            </ul>
          </div>
        </div>
        <div className={styles.intro}>
          <h1 className={styles.intro_title}>Fur Babies</h1>
          <p>Where love finds a home. Join us in fostering, caring, and adopting.</p>
          <a href="#faq" className={styles.intro_button}>Learn More</a>
        </div>
      </div>
    </div>
  );
}
