import Hero from './components/hero/Hero';
import Mission from '@/app/components/mission/Mission'
import MeetUs from './components/meet_us/MeetUs';
import CallToActionForm from './components/call_to_action/CallToActionForm';
import Testimonies from './components/testimonies/Testimonies';
import Stories from './components/stories/Stories';
import Faq from './components/faq/Faq';
import Blog from './components/blog/Blog';
import Stats from './components/stats/Stats'
import WhyUs from './components/why_us/WhyUs';
import CallToActionFull from './components/call_to_action/CallToActionFull';
import Footer from './components/footer/Footer';
export default function Home() {
  return (<div>
      <Hero/>
      <Mission/>
      <MeetUs/>
      <CallToActionForm/>
      <Testimonies/>
      <Stories/>
      <Faq/>
      <Blog/>
      <Stats/>
      <WhyUs/>
      <CallToActionFull/>
      <Footer/>
    </div>
  );
}
